package ar.com.aconcaguasf.exercise.controller.test;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;
import ar.com.aconcaguasf.exercise.util.DatabaseCleanupService;

// Estos tests hacen inserciones concretas en la base de datos (con commit incluido).
// Ver https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
public class BranchOfficeRESTServiceFullITCase {
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private DatabaseCleanupService databaseCleanupService;
	
	@After
	public void cleanDatabase() {
		databaseCleanupService.truncate();
	}
	
	private BranchOfficeReqDTO buenosAiresBranchOffice() {
		return new BranchOfficeReqDTO("Buenos Aires", new CoordinatesDTO(-34.574070,-58.453773));
	}

	private BranchOfficeReqDTO mendozaBranchOffice() {
		return new BranchOfficeReqDTO("Mendoza", new CoordinatesDTO(-32.902683,-68.841404));
	}

	@Test
	public void branchOfficeIsPersistedNicely() {
		// setup
		BranchOfficeReqDTO buenosAiresBranchOffice = buenosAiresBranchOffice();
		
		// exercise
		ResponseEntity<Long> response = restTemplate.postForEntity("/branch-offices", buenosAiresBranchOffice, Long.class);
		
		// verify
		assertEquals(OK, response.getStatusCode());
	}
	
	@Test
	public void aPersistedBranchOfficeIsRetrievedNicely() {
		// setup
		BranchOfficeReqDTO buenosAiresBranchOffice = buenosAiresBranchOffice();
		Long buenosAiresId = restTemplate.postForObject("/branch-offices", buenosAiresBranchOffice, Long.class);
		
		// exercise
		ResponseEntity<BranchOfficeDTO> response = restTemplate.getForEntity("/branch-offices/" + buenosAiresId, BranchOfficeDTO.class);

		// verify
		assertEquals(OK, response.getStatusCode());
		BranchOfficeDTO retrievedBuenosAiresBranchOffice = response.getBody();
		assertEquals(buenosAiresBranchOffice.getName(), retrievedBuenosAiresBranchOffice.getName());
		// Debería implementar equals en CoordinatesDTO y hacer un solo assert... Pero no dispongo de tiempo ahora...
		assertEquals(buenosAiresBranchOffice.getAddress().getLatitude(), retrievedBuenosAiresBranchOffice.getAddress().getLatitude());
		assertEquals(buenosAiresBranchOffice.getAddress().getLongitude(), retrievedBuenosAiresBranchOffice.getAddress().getLongitude());
	}
	
	@Test
	public void retrieveInexistentBranchOfficeEndsInA404Response() {
		// setup
		Long inexistentBranchOfficeId = Long.MAX_VALUE;
		
		// exercise
		ResponseEntity<BranchOfficeDTO> response = restTemplate.getForEntity("/branch-offices/" + inexistentBranchOfficeId, BranchOfficeDTO.class);
		
		assertEquals(NOT_FOUND, response.getStatusCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void allAddedBranchOfficesAreFound() {
		// setup
		BranchOfficeReqDTO buenosAiresBranchOffice = buenosAiresBranchOffice();
		BranchOfficeReqDTO mendozaBranchOffice = mendozaBranchOffice();
		Long buenosAiresId = restTemplate.postForObject("/branch-offices", buenosAiresBranchOffice, Long.class);
		Long mendozaId = restTemplate.postForObject("/branch-offices", mendozaBranchOffice, Long.class);
		
		// exercise
		ResponseEntity<BranchOfficeDTO[]> response = restTemplate.getForEntity("/branch-offices", BranchOfficeDTO[].class);

		// verify
		assertEquals(OK, response.getStatusCode());
		List<BranchOfficeDTO> retrievedBranchOffices = asList(response.getBody());
		assertThat(
				retrievedBranchOffices, 
				hasItems(
						Matchers.<BranchOfficeDTO>hasProperty("id", equalTo(buenosAiresId)), 
						Matchers.<BranchOfficeDTO>hasProperty("id", equalTo(mendozaId))
				)
		);
		assertEquals(2, retrievedBranchOffices.size());
	}
}
