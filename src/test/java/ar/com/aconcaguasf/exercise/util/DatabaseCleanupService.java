package ar.com.aconcaguasf.exercise.util;

import static java.util.stream.Collectors.toSet;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;
import javax.transaction.Transactional;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

// Basado en https://medium.com/@dSebastien/cleaning-up-database-tables-after-each-integration-test-method-with-spring-boot-2-and-kotlin-7279abcdd5cc
// El original estaba en Kotlin
@Service
@Profile("test")
public class DatabaseCleanupService implements InitializingBean {
	private final EntityManager entityManager;
	private Set<String> tableNames;	

	@Autowired
	public DatabaseCleanupService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void afterPropertiesSet() {
		Metamodel metaModel = entityManager.getMetamodel();
		Set<ManagedType<?>> managedTypes = metaModel.getManagedTypes();
		tableNames = managedTypes.stream()
			.filter(mt -> findAnnotation(mt.getJavaType(), Table.class) != null)
			.map(mt -> findAnnotation(mt.getJavaType(), Table.class))
			.map(annotation -> annotation.name())
			.collect(toSet());
	}
	
    @Transactional
    public void truncate() {
        entityManager.flush();
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        tableNames.forEach(tableName -> entityManager.createNativeQuery("TRUNCATE TABLE " + tableName).executeUpdate());
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
    }
}
