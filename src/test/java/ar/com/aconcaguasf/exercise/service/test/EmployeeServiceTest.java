package ar.com.aconcaguasf.exercise.service.test;

import static org.mockito.Mockito.mock;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;
import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;
import ar.com.aconcaguasf.exercise.repository.BranchOfficeRepository;
import ar.com.aconcaguasf.exercise.repository.EmployeeRepository;
import ar.com.aconcaguasf.exercise.service.EmployeeService;
import ar.com.aconcaguasf.exercise.service.impl.EmployeeServiceImpl;

public class EmployeeServiceTest {
	private EmployeeService tested;
	private EmployeeRepository employeeRepository;
	private BranchOfficeRepository branchOfficeRepository;
	
	@Before
	public void setup() {
		this.employeeRepository = mock(EmployeeRepository.class);
		this.branchOfficeRepository = mock(BranchOfficeRepository.class);
		this.tested = new EmployeeServiceImpl(employeeRepository, branchOfficeRepository);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void cantAddAnEmployeeReferingAnInexistentBranchOffice() throws ResourceNotFoundException {
		// setup
		Long inexistentBranchOfficeId = RandomUtils.nextLong();
		Mockito.when(branchOfficeRepository.existsById(inexistentBranchOfficeId)).thenReturn(false);
		EmployeeReqDTO employee = new EmployeeReqDTO("some dummy name", new CoordinatesDTO(0D, 0D), inexistentBranchOfficeId);
		
		// excercise & verify
		tested.add(employee);
	}
}
