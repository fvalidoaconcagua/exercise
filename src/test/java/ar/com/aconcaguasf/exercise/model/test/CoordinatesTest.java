package ar.com.aconcaguasf.exercise.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import ar.com.aconcaguasf.exercise.model.Coordinates;

// Todos los valores que se están testando fueron tomados de https://www.thoughtco.com/degree-of-latitude-and-longitude-distance-4070616

public class CoordinatesTest {
	private void doTestDistance(Coordinates point1, Coordinates point2, int expectedDistance) {
		// exercise
		int distance = point1.distance(point2);
		
		// verify
		assertEquals(expectedDistance, distance);
	}

	private void doTestOneDegreeLatitudeDistance(double latitudeCenter, double longitude, int expectedDistance) {
		// setup
		Coordinates point1 = new Coordinates(latitudeCenter - 0.5D, longitude);
		Coordinates point2 = new Coordinates(latitudeCenter + 0.5D, longitude);
		
		// exercise & verify
		doTestDistance(point1, point2, expectedDistance);
	}
	
	private void doTestOneDegreeLongitudeDistance(double latitude, double longitudeCenter, int expectedDistance) {
		// setup
		Coordinates point1 = new Coordinates(latitude, longitudeCenter - 0.5D);
		Coordinates point2 = new Coordinates(latitude, longitudeCenter + 0.5D);
		
		// exercise & verify
		doTestDistance(point1, point2, expectedDistance);
	}
	
	@Test
	public void twoPointsSeparatedBy1DegreeLatitudeDifferenceAtEquatorAre111KmsAway() {
		// setup & exercise & verify
		doTestOneDegreeLatitudeDistance(0D, 50D, 111);
	}

	@Test
	public void twoPointsSeparatedBy1DegreeLatitudeDifferenceAtTropicOfCapricornAre111KmsAway() {
		// setup & exercise & verify
		doTestOneDegreeLatitudeDistance(-23.5D, 50D, 111);
	}

	@Ignore("El algoritmo no es exacto... no tiene en cuenta el achatamiento en los polos y otras particularidades...")
	@Test
	public void twoPointsSeparatedBy1DegreeLatitudeDifferenceNearSouthPoleAre112KmsAway() {
		// setup & exercise & verify
		doTestOneDegreeLatitudeDistance(-89D, 50D, 112);
	}
	
	@Test
	public void twoPointsSeparatedBy1DegreeLongitudeDifferenceAtEquatorAre111KmsAway() {
		// setup & exercise & verify
		doTestOneDegreeLongitudeDistance(0D, 50D, 111);
	}

	@Test
	public void twoPointsSeparatedBy1DegreeLatitudeDifferenceAt40SouthAre85KmsAway() {
		// setup & exercise & verify
		doTestOneDegreeLongitudeDistance(-40D, 50D, 85);
	}
}
