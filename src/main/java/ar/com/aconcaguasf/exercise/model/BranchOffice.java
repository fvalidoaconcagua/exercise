package ar.com.aconcaguasf.exercise.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.unmodifiableSet;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;

@Entity
@Table(name = "BRANCH_OFFICE")
public class BranchOffice {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Embedded
	@AttributeOverrides({
        @AttributeOverride(name = Coordinates.PropertyNames.LATITUDE, column = @Column(name = "ADDRESS_LATITUDE")),                       
        @AttributeOverride(name = Coordinates.PropertyNames.LONGITUDE, column = @Column(name = "ADDRESS_LONGITUDE"))
    })
	private Coordinates address;
	
	@OneToMany(fetch = LAZY, mappedBy = Employee.PropertyNames.WORKING_OFFICE)
	private Set<Employee> employees;
	
	public BranchOffice(String name, Coordinates address) {
		checkArgument(isNotBlank(name));
		this.name = name;
		this.address = checkNotNull(address);
		this.employees = newHashSet();
	}
	
	// hibernate
	protected BranchOffice() { }
	
	void addEmployee(Employee employee) {
		employees.add(employee);
	}
	
	public Set<Employee> getEmployees() {
		return unmodifiableSet(employees);
	}
	
	public Coordinates getAddress() {
		return address;
	}
	
	public Long getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (!(obj instanceof BranchOffice)) { return false; }
			
		BranchOffice rhs = (BranchOffice) obj;

		// Si no tienen Id asignado, la identidad de los objetos es diferente.
		if (id == null || rhs.getId() == null) { return false; }
		
		return id.equals(rhs.getId());
	}
	
	@Override
	public int hashCode() {
		int initialNonZeroOddNumber = 2563;
		int multiplierNonZeroOddNumber = 37;
		return new HashCodeBuilder(initialNonZeroOddNumber, multiplierNonZeroOddNumber)
			// Asumo que el nombre no puede cambiar... Si esto deja de ser así, habrá que buscar una implementación diferente.
			.append(name)
			.toHashCode();
	}
	
	public BranchOfficeDTO toExternal() {
		return new BranchOfficeDTO(id, name, address.toExternal());
	}
	
	public static BranchOffice fromExternal(BranchOfficeReqDTO external) {
		return new BranchOffice(external.getName(), Coordinates.fromExternal(external.getAddress()));
	}
	
	private BranchOffice(Long id) {
		this.id = id;
		this.employees = newHashSet();
	}
	static BranchOffice fromExternal(EmployeeReqDTO external) {
		return new BranchOffice(external.getWorkingOfficeId());
	}
}
