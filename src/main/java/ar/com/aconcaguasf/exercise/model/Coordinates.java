package ar.com.aconcaguasf.exercise.model;

import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.calculateDistanceInKilometers;
import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.isValidLatitude;
import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.isValidLongitude;
import static com.google.common.base.Preconditions.checkArgument;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;

// Es un value object, debería poner los tributos en final e indicar a JPA cómo llamar al constructor que recibe los argumentos.
// No me alcanza el tiempo, y de todos modos no es un problema ahora porque no estoy exponiendo setters.
@Embeddable
public class Coordinates {
	@Column(name = "LATITUDE", nullable = false)
	private Double latitude;
	
	@Column(name = "LONGITUDE", nullable = false)
	private Double longitude;
	
	public interface PropertyNames {
		String LATITUDE = "latitude";
		String LONGITUDE = "longitude";
	}
	
	public Coordinates(Double latitude, Double longitude) {
		checkArgument(isValidLatitude(latitude));
		checkArgument(isValidLongitude(longitude));
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	// Hibernate
	protected Coordinates() { }
	
	public int distance(Coordinates another) {
		return calculateDistanceInKilometers(latitude, longitude, another.latitude, another.longitude);
	}
	
	public CoordinatesDTO toExternal() {
		return new CoordinatesDTO(latitude, longitude);
	}
	
	public static Coordinates fromExternal(CoordinatesDTO external) {
		return new Coordinates(external.getLatitude(), external.getLongitude());
	}
}
