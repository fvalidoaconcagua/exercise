package ar.com.aconcaguasf.exercise.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.persistence.GenerationType.IDENTITY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import ar.com.aconcaguasf.exercise.dto.EmployeeCompleteDTO;
import ar.com.aconcaguasf.exercise.dto.EmployeeDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;

@Entity
@Table(name = "EMPLOYEE")
public class Employee {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Embedded
	@AttributeOverrides({
        @AttributeOverride(name = Coordinates.PropertyNames.LATITUDE, column = @Column(name = "HOME_LATITUDE")),                       
        @AttributeOverride(name = Coordinates.PropertyNames.LONGITUDE, column = @Column(name = "HOME_LONGITUDE"))
    })
	private Coordinates homeAddress;
	
	@ManyToOne
	@JoinColumn(name = "BRANCH_OFFICE_ID", nullable = false)
	private BranchOffice workingOffice;
	
	public interface PropertyNames {
		String WORKING_OFFICE = "workingOffice";
	}
	
	public Employee(String name, Coordinates homeAddress, BranchOffice workingOffice) {
		checkArgument(isNotBlank(name));
		this.name = name;
		this.homeAddress = checkNotNull(homeAddress);
		this.workingOffice = checkNotNull(workingOffice);
		
		workingOffice.addEmployee(this);
	}
	
	// Hibernate
	protected Employee() { }
	
	public Long getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (!(obj instanceof Employee)) { return false; }
			
		Employee rhs = (Employee) obj;

		// Si no tienen Id asignado, la identidad de los objetos es diferente.
		if (id == null || rhs.getId() == null) { return false; }
		
		return id.equals(rhs.getId());
	}
	
	@Override
	public int hashCode() {
		int initialNonZeroOddNumber = 273;
		int multiplierNonZeroOddNumber = 37;
		return new HashCodeBuilder(initialNonZeroOddNumber, multiplierNonZeroOddNumber)
			// Asumo que el nombre no puede cambiar... Si esto deja de ser así, habrá que buscar una implementación diferente.
			.append(name)
			.toHashCode();
	}
	
	public EmployeeDTO toExternal() {
		return new EmployeeDTO(id, name, homeAddress.toExternal(), this.homeAddress.distance(workingOffice.getAddress()));
	}

	public EmployeeCompleteDTO toExternalComplete() {
		return new EmployeeCompleteDTO(id, name, homeAddress.toExternal(), this.homeAddress.distance(workingOffice.getAddress()), workingOffice.toExternal());
	}
	
	public static Employee fromExternal(EmployeeReqDTO external) {
		return new Employee(
				external.getName(), 
				Coordinates.fromExternal(external.getHomeAddress()), 
				BranchOffice.fromExternal(external)
		);
	}
}
