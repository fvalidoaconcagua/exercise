package ar.com.aconcaguasf.exercise.service;

import java.util.Optional;
import java.util.Set;

import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.EmployeeDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;

public interface BranchOfficeService {
	Long add(BranchOfficeReqDTO branchOfficeDTO);
	Set<BranchOfficeDTO> findAll();
	Optional<BranchOfficeDTO> findById(Long id);
	Optional<Set<EmployeeDTO>> findEmployeesByBranchOfficeId(Long id);
}
