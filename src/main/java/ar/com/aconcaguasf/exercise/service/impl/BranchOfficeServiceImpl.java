package ar.com.aconcaguasf.exercise.service.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toSet;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.EmployeeDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;
import ar.com.aconcaguasf.exercise.model.BranchOffice;
import ar.com.aconcaguasf.exercise.model.Employee;
import ar.com.aconcaguasf.exercise.repository.BranchOfficeRepository;
import ar.com.aconcaguasf.exercise.service.BranchOfficeService;

@Service
public class BranchOfficeServiceImpl implements BranchOfficeService {
	private final BranchOfficeRepository branchOfficeRepository;

	@Autowired
	public BranchOfficeServiceImpl(BranchOfficeRepository branchOfficeRepository) {
		this.branchOfficeRepository = checkNotNull(branchOfficeRepository);
	}
	
	@Transactional
	@Override
	public Long add(BranchOfficeReqDTO branchOfficeDTO) {
		BranchOffice branchOffice = BranchOffice.fromExternal(branchOfficeDTO);
		branchOffice = branchOfficeRepository.save(branchOffice);
		return branchOffice.getId();
	}

	@Override
	public Set<BranchOfficeDTO> findAll() {
		Set<BranchOfficeDTO> branchOffices = newHashSet();
		branchOfficeRepository.findAll().forEach(item -> branchOffices.add(item.toExternal()));
		return branchOffices;
	}

	@Override
	public Optional<BranchOfficeDTO> findById(Long id) {
		return branchOfficeRepository.findById(id).map(BranchOffice::toExternal);
	}

	@Override
	public Optional<Set<EmployeeDTO>> findEmployeesByBranchOfficeId(Long id) {
		return branchOfficeRepository.findById(id)
				.map(BranchOffice::getEmployees)
				.map(set -> set.stream().map(Employee::toExternal).collect(toSet()));
	}
}
