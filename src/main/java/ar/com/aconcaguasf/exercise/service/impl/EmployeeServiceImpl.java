package ar.com.aconcaguasf.exercise.service.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.aconcaguasf.exercise.dto.EmployeeCompleteDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;
import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;
import ar.com.aconcaguasf.exercise.model.Employee;
import ar.com.aconcaguasf.exercise.repository.BranchOfficeRepository;
import ar.com.aconcaguasf.exercise.repository.EmployeeRepository;
import ar.com.aconcaguasf.exercise.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	private final EmployeeRepository employeeRepository;
	private final BranchOfficeRepository branchOfficeRepository;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository employeeRepository, BranchOfficeRepository branchOfficeRepository) {
		this.employeeRepository = checkNotNull(employeeRepository);
		this.branchOfficeRepository = checkNotNull(branchOfficeRepository);
	}
	
	@Transactional
	@Override
	public Long add(EmployeeReqDTO employeeDTO) throws ResourceNotFoundException {
		if (!branchOfficeRepository.existsById(employeeDTO.getWorkingOfficeId())) {
			throw new ResourceNotFoundException();
		}
		Employee employee = Employee.fromExternal(employeeDTO);
		employee = employeeRepository.save(employee);
		return employee.getId();
	}

	@Override
	public Set<EmployeeCompleteDTO> findAll() {
		Set<EmployeeCompleteDTO> employees = newHashSet();
		employeeRepository.findAll().forEach(item -> employees.add(item.toExternalComplete()));
		return employees;
	}

	@Override
	public Optional<EmployeeCompleteDTO> findById(Long id) {
		return employeeRepository.findById(id).map(Employee::toExternalComplete);
	}
}
