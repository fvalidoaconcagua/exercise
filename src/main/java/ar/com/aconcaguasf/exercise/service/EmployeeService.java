package ar.com.aconcaguasf.exercise.service;

import java.util.Optional;
import java.util.Set;

import ar.com.aconcaguasf.exercise.dto.EmployeeCompleteDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;
import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;

public interface EmployeeService {
	Long add(EmployeeReqDTO employeeDTO) throws ResourceNotFoundException;
	Set<EmployeeCompleteDTO> findAll();
	Optional<EmployeeCompleteDTO> findById(Long id);
}
