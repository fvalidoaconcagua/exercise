package ar.com.aconcaguasf.exercise.util;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

public final class CoordinatesUtils {
	private CoordinatesUtils() {
	}

	public static final long MIN_LATITUDE = -90L;
	public static final long MAX_LATITUDE = 90L;

	public static boolean isValidLatitude(Double latitude) {
		return MIN_LATITUDE <= latitude && MAX_LATITUDE >= latitude;
	}

	public static final long MIN_LONGITUDE = -180L;
	public static final long MAX_LONGITUDE = 180L;

	public static boolean isValidLongitude(Double longitude) {
		return MIN_LONGITUDE <= longitude && MAX_LONGITUDE >= longitude;
	}

	public static final double AVERAGE_RADIUS_OF_EARTH_KM = 6378.16;
	
	public static int calculateDistanceInKilometers(double latitude1, double longitude1, double latitude2, double longitude2) {
		// Tomado de https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula/12600225#12600225
		double latitudeDifferenceInRadians = toRadians(latitude1 - latitude2);
		double longitudeDifferenceInRadians = toRadians(longitude1 - longitude2);

		double a = sin(latitudeDifferenceInRadians / 2) * sin(latitudeDifferenceInRadians / 2) + cos(toRadians(latitude1))
				* cos(toRadians(latitude2)) * sin(longitudeDifferenceInRadians / 2) * sin(longitudeDifferenceInRadians / 2);

		double c = 2 * atan2(sqrt(a), sqrt(1 - a));

		return (int) (round(AVERAGE_RADIUS_OF_EARTH_KM * c));
	}
}
