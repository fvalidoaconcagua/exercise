package ar.com.aconcaguasf.exercise.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.aconcaguasf.exercise.model.BranchOffice;

// No me gusta los repositorios autómaticos porque permiten acciones que no solo no serán usadas sino
// que quizás incluso no DEBERÍA ser usada. Y si bien es algo "retorcido" (expresión Argentina: rebuscado
// podría ser un equivalente), bien podría pasar que un programador invoque esa acción viendo que está
// ahí, disponible. Prefiero entonces tener repositorios "custom" implementados "manualmente".
// NOTA: Por otra parte, este código automático ya está testeado y es "gratis" (sin esfuerzo), lo cual
//       no es nada despreciable.
// Como sea, por cuestiones de disponibilidad de tiempo para realizar este ejercicio, en este caso lo
// usaré.
@Repository
public interface BranchOfficeRepository extends CrudRepository<BranchOffice, Long> {
}
