package ar.com.aconcaguasf.exercise.controller.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.aconcaguasf.exercise.controller.BranchOfficeRESTService;
import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.EmployeeDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;
import ar.com.aconcaguasf.exercise.service.BranchOfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(tags = "Sucursales")
public class BranchOfficeRESTServiceImpl implements BranchOfficeRESTService {
	private final BranchOfficeService branchOfficeService;

	@Autowired
	public BranchOfficeRESTServiceImpl(BranchOfficeService branchOfficeService) {
		this.branchOfficeService = checkNotNull(branchOfficeService);
	}
	
	@ApiOperation("Agrega una sucursal.")
	@ApiResponses(
		@ApiResponse(code = 200, message = "Código de la sucursal creada.")
	)
	@RequestMapping(method = POST, value = "/branch-offices")
	@Override
	public Long addBranchOffice(
			@Valid @RequestBody BranchOfficeReqDTO branchOffice
	) {
		return branchOfficeService.add(branchOffice);
	}

	@ApiOperation("Obtiene todas las sucursales.")
	@RequestMapping(method = GET, value = "/branch-offices")
	@Override
	public Set<BranchOfficeDTO> findAllBranchOffices() {
		return branchOfficeService.findAll();
	}

	@ApiResponses(
		@ApiResponse(code = 404, message = "No se encontró.")
	)
	@ApiOperation("Obtiene una sucursal.")
	@RequestMapping(method = GET, value = "/branch-offices/{id}")
	@Override
	public BranchOfficeDTO findBranchOfficeById(
			@PathVariable("id") Long id
	) {
		return branchOfficeService.findById(id).orElse(null);
	}

	@ApiResponses(
		@ApiResponse(code = 404, message = "No se encontró.")
	)
	@ApiOperation("Obtiene los empleados de una sucursal.")
	@RequestMapping(method = GET, value = "/branch-offices/{id}/employees")
	@Override
	public Set<EmployeeDTO> findEmployeesOnBranchOfficeById(
			@PathVariable("id") Long id
	) {
		return branchOfficeService.findEmployeesByBranchOfficeId(id).orElse(null);
	}
}
