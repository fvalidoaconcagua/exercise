package ar.com.aconcaguasf.exercise.controller;

import java.util.Set;

import ar.com.aconcaguasf.exercise.dto.BranchOfficeDTO;
import ar.com.aconcaguasf.exercise.dto.EmployeeDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;

public interface BranchOfficeRESTService {
	Long addBranchOffice(BranchOfficeReqDTO branchOffice);
	Set<BranchOfficeDTO> findAllBranchOffices();
	BranchOfficeDTO findBranchOfficeById(Long id);
	Set<EmployeeDTO> findEmployeesOnBranchOfficeById(Long id);
}
