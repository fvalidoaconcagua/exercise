package ar.com.aconcaguasf.exercise.controller;

import java.util.Set;

import ar.com.aconcaguasf.exercise.dto.EmployeeCompleteDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;
import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;

public interface EmployeeRESTService {
	Long addEmployee(EmployeeReqDTO employeeOffice) throws ResourceNotFoundException;
	Set<EmployeeCompleteDTO> findAllEmployees();
	EmployeeCompleteDTO findEmployeeById(Long id);
}
