package ar.com.aconcaguasf.exercise.controller.config;

import static com.google.common.base.Predicates.not;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
	private ResponseMessage default400() {
		return new ResponseMessage(BAD_REQUEST.value(), "Invocacion errónea.", null, emptyMap(), emptyList());
	}

	private ResponseMessage default500() {
		return new ResponseMessage(INTERNAL_SERVER_ERROR.value(), "Error interno.", null, emptyMap(), emptyList());
	}
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.globalResponseMessage(
						DELETE,
						asList(default400(), default500()))
				.globalResponseMessage(
						POST,
						asList(default400(), default500()))
				.globalResponseMessage(
						PUT,
						asList(default400(), default500()))
				.globalResponseMessage(
						GET,
						asList(default500()))
				.globalResponseMessage(
						PATCH,
						asList(default400(), default500()))
				.select()
				.apis(not(basePackage("org.springframework.boot")))
				.paths(any())
				.build();
	}
}
