package ar.com.aconcaguasf.exercise.controller.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.aconcaguasf.exercise.controller.EmployeeRESTService;
import ar.com.aconcaguasf.exercise.dto.EmployeeCompleteDTO;
import ar.com.aconcaguasf.exercise.dto.req.EmployeeReqDTO;
import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;
import ar.com.aconcaguasf.exercise.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(tags = "Empleados")
public class EmployeeRESTServiceImpl implements EmployeeRESTService {
	private final EmployeeService employeeService;

	@Autowired
	public EmployeeRESTServiceImpl(EmployeeService employeeService) {
		this.employeeService = checkNotNull(employeeService);
	}
	
	@ApiOperation("Agrega un empleado.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Código del empleado creado."),
		@ApiResponse(code = 404, message = "No se encontró la sucursal.")
	})
	@RequestMapping(method = POST, value = "/employees")
	@Override
	public Long addEmployee(
			@Valid @RequestBody EmployeeReqDTO employee
	) throws ResourceNotFoundException {
		return employeeService.add(employee);
	}

	@ApiOperation("Obtiene todos los empleados.")
	@RequestMapping(method = GET, value = "/employees")
	@Override
	public Set<EmployeeCompleteDTO> findAllEmployees() {
		return employeeService.findAll();
	}

	@ApiResponses(
		@ApiResponse(code = 404, message = "No se encontró.")
	)
	@ApiOperation("Obtiene un empleado.")
	@RequestMapping(method = GET, value = "/employees/{id}")
	@Override
	public EmployeeCompleteDTO findEmployeeById(
			@PathVariable("id") Long id
	) {
		return employeeService.findById(id).orElse(null);
	}
}
