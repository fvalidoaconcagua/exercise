package ar.com.aconcaguasf.exercise.controller.config;

import static com.google.common.collect.Maps.newHashMap;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;

@Component
public class CustomErrorAttributes extends DefaultErrorAttributes {
	private static final String JAVAX_SERVLET_ERROR_STATUS_CODE = "javax.servlet.error.status_code";
	private static final String RECURSO_NO_ENCONTRADO = "Recurso no encontrado.";
    private static final String MESSAGE_KEY = "message";

    private final Map<Class<? extends Throwable>, HttpStatus> customStatus;
	private final Map<Class<? extends Throwable>, String> customMessages;
    
    public CustomErrorAttributes() {
    	customStatus = newHashMap();
    	customStatus.put(ResourceNotFoundException.class, NOT_FOUND);
    	customMessages = newHashMap();
    	customMessages.put(ResourceNotFoundException.class, RECURSO_NO_ENCONTRADO);
	}
	
	@Override
    public Map<String, Object> getErrorAttributes(WebRequest requestAttributes, boolean includeStackTrace) {
        Throwable error = getError(requestAttributes);
        if (error instanceof UndeclaredThrowableException) {
        	error = ((UndeclaredThrowableException) error).getUndeclaredThrowable();
        }
        if (error != null && customStatus.containsKey(error.getClass())) {
        	requestAttributes.setAttribute(JAVAX_SERVLET_ERROR_STATUS_CODE, customStatus.get(error.getClass()).value(), 0);
        }
    	
        Map<String, Object> response = super.getErrorAttributes(requestAttributes, includeStackTrace);
        
    	if (error != null && customMessages.containsKey(error.getClass())) {
        	response.put(MESSAGE_KEY, customMessages.get(error.getClass()));
        }
        
    	return response;
    }
}
