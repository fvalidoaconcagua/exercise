package ar.com.aconcaguasf.exercise.controller.config;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import ar.com.aconcaguasf.exercise.exception.ResourceNotFoundException;

// Tomado de https://stackoverflow.com/questions/43888003/return-404-for-every-null-response
@Aspect
@Component
public class ResourceNotFoundRESTResponse {
	@AfterReturning(pointcut = "execution(* ar.com.aconcaguasf.exercise.controller.*RESTService+.find*ById(..))", returning = "result")
	public void intercept(final Object result) throws ResourceNotFoundException {
		if (result == null) {
			throw new ResourceNotFoundException();
		}
	}
}
