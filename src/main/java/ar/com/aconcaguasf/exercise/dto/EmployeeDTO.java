package ar.com.aconcaguasf.exercise.dto;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import io.swagger.annotations.ApiModelProperty;

public class EmployeeDTO {
	@ApiModelProperty(value = "Id del empleado", required = true)
	private final Long id;

	@ApiModelProperty(value = "Nombre del empleado", required = true)
	private final String name;

	@ApiModelProperty(value = "Domicilio del empleado", required = true)
	private final CoordinatesDTO homeAddress;

	@ApiModelProperty(value = "Distancia en kms desde la oficina en la que trabaja a su domicilio", required = true)
	private final Integer distanceHomeWork;
	
	public EmployeeDTO(Long id, String name, CoordinatesDTO homeAddress, Integer distanceHomeWork) {
		this.id = id;
		this.name = name;
		this.homeAddress = homeAddress;
		this.distanceHomeWork = distanceHomeWork;
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public CoordinatesDTO getHomeAddress() {
		return homeAddress;
	}

	public Integer getDistanceHomeWork() {
		return distanceHomeWork;
	}
}
