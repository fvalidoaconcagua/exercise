package ar.com.aconcaguasf.exercise.dto.common;

import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.MAX_LATITUDE;
import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.MAX_LONGITUDE;
import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.MIN_LATITUDE;
import static ar.com.aconcaguasf.exercise.util.CoordinatesUtils.MIN_LONGITUDE;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class CoordinatesDTO {
	@NotNull
	@Min(MIN_LATITUDE)
	@Max(MAX_LATITUDE)
	@ApiModelProperty(value = "Coordenada de latitud", required = true)
	private Double latitude;

	@NotNull
	@Min(MIN_LONGITUDE)
	@Max(MAX_LONGITUDE)
	@ApiModelProperty(value = "Coordenada de longitud", required = true)
	private Double longitude;
	
	public CoordinatesDTO(Double latitude, Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	protected CoordinatesDTO() { }	
	
	protected void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	protected void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
}
