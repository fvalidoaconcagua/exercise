package ar.com.aconcaguasf.exercise.dto;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import ar.com.aconcaguasf.exercise.dto.req.BranchOfficeReqDTO;
import io.swagger.annotations.ApiModelProperty;

public class BranchOfficeDTO extends BranchOfficeReqDTO {
	@ApiModelProperty(value = "Id de la sucursal", required = true)
	private Long id;
	
	public BranchOfficeDTO(Long id, String name, CoordinatesDTO address) {
		super(name, address);
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	
	protected BranchOfficeDTO() { }
	
	protected void setId(Long id) {
		this.id = id;
	}
}
