package ar.com.aconcaguasf.exercise.dto.req;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import io.swagger.annotations.ApiModelProperty;

public class BranchOfficeReqDTO {
	@NotEmpty
	@ApiModelProperty(value = "Nombre de la oficina", required = true)
	private String name;
	
	@NotNull
	@ApiModelProperty(value = "Coordendas de la ubicación de la oficina")
	@Valid
	private CoordinatesDTO address;
	
	public BranchOfficeReqDTO(String name, CoordinatesDTO address) {
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public CoordinatesDTO getAddress() {
		return address;
	}

	protected BranchOfficeReqDTO() { }

	protected void setName(String name) {
		this.name = name;
	}

	protected void setAddress(CoordinatesDTO address) {
		this.address = address;
	}
}
