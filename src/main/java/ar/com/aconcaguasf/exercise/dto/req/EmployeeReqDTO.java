package ar.com.aconcaguasf.exercise.dto.req;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import io.swagger.annotations.ApiModelProperty;

public class EmployeeReqDTO {
	@NotEmpty
	@ApiModelProperty(value = "Nombre del empleado", required = true)
	private String name;
	
	@NotNull
	@ApiModelProperty(value = "Domicilio del empleado", required = true)
	@Valid
	private CoordinatesDTO homeAddress;
	
	@NotNull
	@ApiModelProperty(value = "Id de la oficina en la que se desempeña el empleado", required = true)
	private Long workingOfficeId;
	
	public EmployeeReqDTO(String name, CoordinatesDTO homeAddress, Long workingOfficeId) {
		this.name = name;
		this.homeAddress = homeAddress;
		this.workingOfficeId = workingOfficeId;
	}

	public String getName() {
		return name;
	}

	public CoordinatesDTO getHomeAddress() {
		return homeAddress;
	}
	
	public Long getWorkingOfficeId() {
		return workingOfficeId;
	}

	protected EmployeeReqDTO() { }

	protected void setName(String name) {
		this.name = name;
	}

	protected void setHomeAddress(CoordinatesDTO homeAddress) {
		this.homeAddress = homeAddress;
	}
	
	protected void setWorkingOfficeId(Long workingOfficeId) {
		this.workingOfficeId = workingOfficeId;
	}
}
