package ar.com.aconcaguasf.exercise.dto;

import ar.com.aconcaguasf.exercise.dto.common.CoordinatesDTO;
import io.swagger.annotations.ApiModelProperty;

public class EmployeeCompleteDTO extends EmployeeDTO {
	@ApiModelProperty(value = "Sucursal en la que se desempeña el empleado", required = true)
	private final BranchOfficeDTO workingOffice;
	
	public EmployeeCompleteDTO(Long id, String name, CoordinatesDTO homeAddress, Integer distanceHomeWork, BranchOfficeDTO workingOffice) {
		super(id, name, homeAddress, distanceHomeWork);
		this.workingOffice = workingOffice;
	}

	public BranchOfficeDTO getWorkingOffice() {
		return workingOffice;
	}
}
